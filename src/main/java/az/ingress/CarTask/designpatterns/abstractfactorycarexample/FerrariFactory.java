package az.ingress.CarTask.designpatterns.abstractfactorycarexample;

public class FerrariFactory implements ClassicCarsFactory{

    @Override
    public ClassicCars getCar(String model, Integer year, String color) {
        return new Ferrari(model, year, color);
    }
}
