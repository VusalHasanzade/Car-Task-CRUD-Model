package az.ingress.CarTask.designpatterns.abstractfactory;

public interface Color {
    String getColor();
}
