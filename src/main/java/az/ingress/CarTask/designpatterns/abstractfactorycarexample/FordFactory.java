package az.ingress.CarTask.designpatterns.abstractfactorycarexample;

public class FordFactory implements ClassicCarsFactory{

    @Override
    public ClassicCars getCar(String model, Integer year, String color) {
        return new Ford(model, year, color);
    }
}
