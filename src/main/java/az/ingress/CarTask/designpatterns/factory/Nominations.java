package az.ingress.CarTask.designpatterns.factory;

public class Nominations {

    public static GetMedal getMedal(int place) {
        GetMedal getMedal = null;

        switch (place) {
            case 1:
                getMedal = new first();
                break;
            case 2:
                getMedal = new second();
                break;
            case 3:
                getMedal = new third();
                break;
            default:
                break;
        }
        return getMedal;
    }
}