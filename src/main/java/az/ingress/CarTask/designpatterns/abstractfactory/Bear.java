package az.ingress.CarTask.designpatterns.abstractfactory;

public class Bear implements Animal{

    @Override
    public String getAnimal() {
        return "whoa whoa";
    }

    @Override
    public String makeSound() {
        return "bearing";
    }
}
