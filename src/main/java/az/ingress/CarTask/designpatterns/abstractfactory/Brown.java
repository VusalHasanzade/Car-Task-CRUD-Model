package az.ingress.CarTask.designpatterns.abstractfactory;

public class Brown implements Color{
    @Override
    public String getColor() {
        return "brown";
    }
}
