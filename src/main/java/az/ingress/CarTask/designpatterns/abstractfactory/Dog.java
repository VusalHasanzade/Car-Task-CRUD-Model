package az.ingress.CarTask.designpatterns.abstractfactory;

public class Dog implements Animal{
    @Override
    public String getAnimal() {
        return "haw haw";
    }

    @Override
    public String makeSound() {
        return "barking";
    }
}
