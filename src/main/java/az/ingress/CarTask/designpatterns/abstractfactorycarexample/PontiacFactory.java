package az.ingress.CarTask.designpatterns.abstractfactorycarexample;

public class PontiacFactory implements ClassicCarsFactory{
    @Override
    public ClassicCars getCar(String model, Integer year, String color) {
        return new Pontiac(model, year, color);
    }
}
