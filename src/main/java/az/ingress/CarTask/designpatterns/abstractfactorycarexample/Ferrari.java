package az.ingress.CarTask.designpatterns.abstractfactorycarexample;

import lombok.AllArgsConstructor;

@AllArgsConstructor
public class Ferrari implements ClassicCars{

    private String model;
    private Integer year;
    private String color;

    @Override
    public String getModel() {
        return null;
    }

    @Override
    public Integer getYear() {
        return null;
    }

    @Override
    public String getColor() {
        return null;
    }
}
