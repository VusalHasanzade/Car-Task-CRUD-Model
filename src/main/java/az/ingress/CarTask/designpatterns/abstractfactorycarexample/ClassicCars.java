package az.ingress.CarTask.designpatterns.abstractfactorycarexample;

public interface ClassicCars{

    String getModel();
    Integer getYear();
    String getColor();

}
