package az.ingress.CarTask.designpatterns.abstractfactorycarexample;

public interface ClassicCarsFactory {
    ClassicCars getCar(String model,Integer year,String color);
}
