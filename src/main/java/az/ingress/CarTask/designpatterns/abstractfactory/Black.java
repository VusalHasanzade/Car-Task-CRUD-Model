package az.ingress.CarTask.designpatterns.abstractfactory;

public class Black implements Color{
    @Override
    public String getColor() {
        return "black";
    }
}
