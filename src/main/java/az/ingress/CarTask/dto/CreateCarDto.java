package az.ingress.CarTask.dto;


import lombok.Data;

@Data
public class CreateCarDto {

    private String color;
    private Double engine;
    private String model;
    private Integer year;
}
