package az.ingress.CarTask.designpatterns.abstractfactorycarexample;

public class CarGallery {
    public static void main(String[] args) {
        FordFactory fordFactory = new FordFactory();
        ClassicCars ford = fordFactory.getCar("Ford Mustang Mach 1",1969,"grey");
        System.out.println(ford);

        FerrariFactory ferrariFactory = new FerrariFactory();
        ClassicCars ferrari = ferrariFactory.getCar("Ferrari 308 GTS",1975,"red");

        PontiacFactory pontiacFactory = new PontiacFactory();
        ClassicCars pontiac = pontiacFactory.getCar("Pontiac GTO",1967,"black");
    }
}
