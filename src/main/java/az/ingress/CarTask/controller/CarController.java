package az.ingress.CarTask.controller;

import az.ingress.CarTask.dto.CarDto;
import az.ingress.CarTask.dto.CreateCarDto;
import az.ingress.CarTask.dto.UpdateCarDto;
import az.ingress.CarTask.model.Car;
import az.ingress.CarTask.service.CarService;
import org.springframework.data.crossstore.ChangeSet;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping
public class CarController {

    private final CarService carService;

    public CarController(CarService carService) {
        this.carService = carService;
    }

    @PostMapping
    public void create(@RequestBody CreateCarDto dto){
        carService.createCar(dto);
    }

    @PutMapping
    public void update(@RequestBody UpdateCarDto dto){
        carService.updateCar(dto);
    }

    @GetMapping("/{id}")
    public CarDto get(@PathVariable Integer id){
        return carService.getCar(id);
    }

    @DeleteMapping("/{id}")
        public void delete(@PathVariable Integer id) {
        carService.deleteCar(id);
    }

        @GetMapping
        public List<CarDto> getAllCars() {
            return carService.getAllCars();
        }
}
