package az.ingress.CarTask.designpatterns.factory;

public class CompetitionWinners {
    public static void main(String[] args) {
        GetMedal first = Nominations.getMedal(1);
        System.out.println(first.getMedal());

        GetMedal second = Nominations.getMedal(2);
        System.out.println(second.getMedal());

        GetMedal third = Nominations.getMedal(3);
        System.out.println(third.getMedal());
    }
}
