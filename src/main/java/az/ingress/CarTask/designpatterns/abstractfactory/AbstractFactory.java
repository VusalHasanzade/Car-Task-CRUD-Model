package az.ingress.CarTask.designpatterns.abstractfactory;

public interface AbstractFactory <T> {
    T create(String animalType);
}
