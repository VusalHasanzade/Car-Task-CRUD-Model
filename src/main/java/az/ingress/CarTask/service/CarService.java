package az.ingress.CarTask.service;

import az.ingress.CarTask.dto.CarDto;
import az.ingress.CarTask.dto.CreateCarDto;
import az.ingress.CarTask.dto.UpdateCarDto;
import az.ingress.CarTask.model.Car;
import az.ingress.CarTask.repository.CarRepository;
import org.modelmapper.ModelMapper;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
@Primary
public class CarService {

    public final CarRepository carRepository;
    public final ModelMapper modelMapper;

    public CarService(CarRepository carRepository, ModelMapper modelMapper) {
        this.carRepository = carRepository;
        this.modelMapper = modelMapper;
    }

    public void createCar(CreateCarDto createCarDto) {
        Car car = modelMapper.map(createCarDto,Car.class);
        carRepository.save(car);
    }

    public void updateCar(UpdateCarDto dto) {
    }

    public CarDto getCar(Integer id) {
        Car car = carRepository.findById(id).get();
        CarDto carDto = modelMapper.map(car,CarDto.class);
        return carDto;
    }

    public void deleteCar(Integer id) {
        carRepository.deleteById(id);

    }

    public List <CarDto> getAllCars() {
        List <Car> cars=carRepository.findAll();
        List <CarDto> carDTO= new ArrayList<>();

        cars.forEach(car -> carDTO.add(modelMapper.map(car, CarDto.class)));
        return carDTO;
    }
}
