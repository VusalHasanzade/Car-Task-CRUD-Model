package az.ingress.CarTask.designpatterns.abstractfactory;

    public interface Animal {
        String getAnimal();
        String makeSound();
}
